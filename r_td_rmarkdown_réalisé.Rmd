---
title: "corrcetion td2R"
output:
  html_document: default
  pdf_document: default
date: "2022-10-14"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# R graphics

### chargements

chargements des **packages**

```{r packages, warning=FALSE, include=FALSE}
library(dplyr)
library(ggplot2)
```

chargements des données

```{r data, echo=FALSE, warning=FALSE, cache=TRUE}
path.date="C:/Users/sixti/git/r_td2/exercice"
data_src=read.csv2(file.path(path.date,"data_220929.csv"))
head (data_src)
```

Chargement des données et data management

```{r DM, echo=TRUE, warning=FALSE, dependson='data'}
data_src = read.csv2("data_220929.csv")
data_src$sexe = factor(data_src$sexe)
data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")
data = data_src
```
### Questions

Q1 : Histogramme du poids

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..)) 
```

Commentaires : On peut donc observer que la majorité des personnes ont un poids entre 60 et 80 kg

Q2 : Histogramme du poids avec un axe des absisses comprenant les valeurs entre 0 et 150

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..)) +
  xlim(0, 150)
```

Q3 : Courbe de densité du poids en fonction du sexe Il faut supprimer au préalable les poids \> 200

```{r courbe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(poids, colour = sexe)) +
  geom_density(adjust = 1)
```

Commentaire : on peut observer que les femmes on un poids moins élevé que les hommes en général

Q4 : Q3 + thème avec un fond blanc

```{r courbe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(poids, colour = sexe)) +
  geom_density(adjust = 1) +
  theme_bw()
```

Q5 : Boxplots représentant la durée d'anesthésie par classe ASA

```{r boxplots, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(duree_anesthesie < 1000) %>%
  ggplot(aes(duree_anesthesie, asa)) +
  geom_boxplot() +
  coord_flip()
```

Commentaire : La durée d'anesthésie est plus longue avec la classe Asa5 comparé aux autres classes

Q6 : Représentation du nombre d'intervention par classe d'âge : [0-17[, [18-34[, [35-54[, [55-74[, \>74

```{r classe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age)) +
  geom_bar() +
  coord_flip()
```

Commentaire: On peut observer que les personnes netre 35 et 55 subisssent plus d'opérations que les autres

Q7 : Q6 avec proposition de nouvelles couleurs

```{r classe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age, fill = classe_age)) +
  geom_bar() +
  coord_flip() +
  scale_fill_manual(values = c("#CD5C5C", "#F08080", "#FA8072", "#E9967A", "#FFA07A"))
```

Q8 : Q7 avec titre du graphique "Nombre d'interventions par classe d'âge" Retirer les libellés sur axes des abscisses et des ordonnées

```{r classe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age, fill = classe_age)) +
  geom_bar() +
  coord_flip() +
  scale_fill_manual(values = c("#CD5C5C", "#F08080", "#FA8072", "#E9967A", "#FFA07A")) +
  xlab("") +
  ylab("") +
  ggtitle("Nombre d'interventions par classe d'âge")
```

Q9 : Représentation de la mortalité par sexe

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
prop_mortalite_sexe = round(prop.table(table(data$sexe, data$deces_sejour_interv), 1) * 100, 2)
df_mortalite_sexe = as.data.frame(prop_mortalite_sexe) %>%
  filter(Var2 == 1) %>%
  rename(freq_deces = `Freq`,
         sexe = Var1)

df_mortalite_sexe %>%
  ggplot(aes(x = sexe, y = freq_deces)) +
  geom_bar(stat = "identity")
```

Commentaire : On peut observer qu'il y a plus de mort chez les hommes

Q10 : Représentation de l'IMC en fonction de la classe ASA IMC = Poids / Taille (m) ²

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data %>%
  filter(!is.na(poids) & !is.na(taille) & age > 15) %>%
  mutate(imc = round(poids / (taille/100*taille/100))) %>%
  filter(imc < 50 & imc > 10) %>%
  ggplot(aes(asa, imc)) +
  geom_boxplot()
```

Commentaire: On peut observer que l'imc est plus fort pour ASA4

Q11 : Représentation de la mortalité en fonction d'un IMC \> 30 kg/m²

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data_filtre =data %>%
  filter(!is.na(poids) & !is.na(taille)) %>%
  mutate(imc = round(poids / (taille/100*taille/100))) %>%
  filter(imc < 50 & imc > 10) %>%
  mutate(imc_30 = ifelse(imc > 30, 1, 0))
prop_mortalite_imc = round(prop.table(table(data_filtre$imc_30, data_filtre$deces_sejour_interv), 1) * 100, 2)

df_mortalite_imc = as.data.frame(prop_mortalite_imc) %>%
  filter(Var2 == 1) %>%
  rename(freq_deces = `Freq`,
         imc_30 = Var1)

df_mortalite_imc %>%
  mutate(imc_30_label = sub (0 , "Non" , imc_30)) %>%
  mutate(imc_30_label = sub (1 , "Oui" , imc_30_label)) %>%
  ggplot(aes(imc_30_label, freq_deces)) +
  geom_bar(stat = "identity") +
  ylab("Fréquence de décès") + xlab("IMC > 30")
```

Commentaire : plus de chance de décès avec un IMC supérieur à 30

Q12 : Réprésenter la mortalité en fonction de IMC \> 30 kg/m² et categorie_asa

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data_filtre %>%
  group_by(categorie_asa, imc_30, deces_sejour_interv) %>%
  summarize(n = n(),
            )

prop_asa_imc_deces = round(prop.table(table(data_filtre$imc_30, 
                                      data_filtre$categorie_asa, 
                                      data_filtre$deces_sejour_interv), 1) * 100, 2)

df_asa_imc_deces = as.data.frame(prop_asa_imc_deces) %>%
  rename(freq_asa = `Freq`,
         imc_30 = Var1,
         'Categorie_ASA' = Var2) %>%
  filter(Var3 == 1)

df_asa_imc_deces %>%
  mutate(imc_30_label = sub (0 , "Non" , imc_30)) %>%
  mutate(imc_30_label = sub (1 , "Oui" , imc_30_label)) %>%
  ggplot(aes(imc_30_label, freq_asa, fill = Categorie_ASA)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("Fréquence") + xlab("IMC > 30")

```

Commentaire : PLus de chance de décès avec un IMC supérieur à 30 et si on est dans une catégoriee ASA3-4-5

Question 13 : durée de séjour en fonction de la classe asa

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data%>% 
  ggplot (aes(asa,duree_sejour_interv))+
  geom_boxplot()
```

commentaire : la durée du séjour est plus long en majorité pour ceux qui font parti de la catégorie ASA4

Question 14 : durée de séjour en fonction de la classe asa et du sexe

```{r histo, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data%>%
  ggplot (aes(asa,duree_sejour_interv,fill=sexe))+
  geom_boxplot()
```

Commentaire: la durée du séjour est plus long en majorité pour ceux qui font parti de la catégorie ASA4 et si on est un homme cette durée est encore plus longue
